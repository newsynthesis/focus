import {sitePathname} from '../config.js'
import {Router} from './index.js'

import PageHome from '../pages/home.js'
import PageMenu from '../pages/menu.js'
import PageAccount from '../pages/account.js'
import PageGame from '../pages/game.js'
import PageRTC from '../pages/rtc.js'

customElements.define('page-home', PageHome)
customElements.define('page-menu', PageMenu)
customElements.define('page-account', PageAccount)
customElements.define('page-game', PageGame)
customElements.define('page-rtc', PageRTC)

export default class AppRouter extends HTMLElement {
	connectedCallback() {
		this.newRouter()
	}
	newRouter = () => {
		// select the DOM node where the router inserts route Web Components
		const outlet = this
		const router = new Router(outlet, {
			baseUrl: sitePathname
		})
		router.setRoutes([
			{path: '/', component: 'page-home'},
			{path: '/menu', component: 'page-menu'},
			{path: '/account', component: 'page-account'},
			{path: '/game', component: 'page-game'},
			{path: '/rtc', component: 'page-rtc'},
		])
	}
}
