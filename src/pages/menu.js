import {$logger} from '../index.js'

/* a message */
const message = "LS0tLS1CRUdJTiBQR1AgTUVTU0FHRS0tLS0tCgp3eTRFQ1FNSVBSTzNQWk4rRG1yZzNFRUo5eWFuS05wT2dJMXRhTy9PVkFjWHhCOFp4bUcyaUdZR3RNUjcKdk1PVzBxa0J5ZS9DRFF6dDJ1TU13bGZZQytzalA3K3kxS3dGVmRlR2RyODAyOXVsS21mREhoeEJ4UVo1CmNsQ1dtbVh3MGN5akVsWXUwanVhYmcvQ1NZZWdnV1BSKzZQRDlETHRvMnE4OWsvUHV5a2I1SlpBM2xXdQpSK1VtQm1VOWFMVHJvVmhGMHlnRmpVUHVZTkRYM0xaalFXZFZrQU9XcWFRTUgzNlRrbjhjc1pXSUlmcU4KdzVHY0hKME9rU0t0aTdpUGFTRzJnQ21kY3FvWk1HMDFaU1BHc2NQelZFcThkeS9qZ0ZhNgo9aXZaSgotLS0tLUVORCBQR1AgTUVTU0FHRS0tLS0tCg=="

export default class PageHome extends HTMLElement {
	connectedCallback() {
		this.render()
	}
	render = () => {
		const $cryptInput = document.createElement('input-crypt')
		$cryptInput.setAttribute('message', message)

		const $app = this
		$app.prepend($cryptInput)
		$app.prepend($logger)
	}
}
