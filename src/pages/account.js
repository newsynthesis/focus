const AccountPage = class extends HTMLElement {
	connectedCallback() {
		this.render()
	}

	render() {
		let $rsWidget = document.createElement('rs-widget')
		this.append($rsWidget)
		$rsWidget.remoteWidget.open()
		$rsWidget.remoteWidget.leaveOpen = true

		const $loginBtn = this.querySelector('button.rs-choose-rs')
		if ($loginBtn) {
			$loginBtn.focus()
		} else {
			this.querySelector('button.rs-sync').focus()
		}
	}
}

export default AccountPage
