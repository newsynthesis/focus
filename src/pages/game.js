const styles = `
:host {
  height: 100%;
  --bg: white;
  --fg: black;
}
details {
	position: absolute;
	top: 0;
	left: 0;
}
details[open] {
	max-width: 20em;
	max-height: 100vh;
	overflow-y: scroll;
	border-bottom: 0.5em solid var(--fg);
	background-color: var(--bg);
}
summary {
	cursor: pointer;
	padding: 0.5em;
	color: black;
}
#map {
	height: 100%;
}
game-map {
	overflow: hidden;
	height: 100%;
	display: block;
}
foreignObject {
	overflow: visible;
}
`
const template = `
	<style>${styles}</style>
	<div id="user">
		<slot name="user"></slot>
	</div>
	<div id="map">
		<slot name="map"></slot>
	</div>
`

export default class PageGame extends HTMLElement {
	static get observedAttributes() {
		return ['data']
	}
	set data(obj) {
		this.setAttribute('data', JSON.stringify(obj))
	}
	get data() {
		return JSON.parse(this.getAttribute('data'))
	}
	constructor() {
		super()
		this.addEventListener('userPosition', this.onLocalUserPosition)
	}
	attributeChangedCallback(name, newVal, oldVal) {
		console.log(name, newVal, oldVal)
	}
	connectedCallback() {
		this.$root = this.attachShadow({mode: 'open'})
		this.$root.innerHTML = template
		this.data = [
			{
				x: 200,
				y: 200,
				z: 0,
			},
			{
				x: 400,
				y: 400,
				z: 0,
			}
		]
		this.renderUser()
		this.renderMap()
	}
	/* when an attribute changed */
	attributeChangedCallback(attrName, oldVal, newVal) {
		if (attrName === 'data') {
			this.renderMap()
		}
	}
	onLocalUserPosition({detail}) {
		const {newPos} = detail
		if (newPos) {
			const gameData = {
				type: 'gamedata',
				position: newPos,
			}
			if (this.$rtcUser) {
				this.$rtcUser.sendMessage(gameData)
			}
			this.data = [...this.data, newPos]
		}
	}
	onMessage({detail}) {
		const {type, position} = detail
		if (type === 'gamedata') {
			this.data = [...this.data, position]
		}
	}
	renderUser = () => {
		this.$rtcUser = document.createElement('rtc-user')
		this.$rtcUser.setAttribute('manual-signaling', true)
		this.$rtcUser.addEventListener('message', this.onMessage.bind(this))

		const $details = document.createElement('details')
		const $summary = document.createElement('summary')
		$summary.innerText = 'peer'
		$details.append($summary)
		$details.append(this.$rtcUser)
		this.$root.querySelector('#user').append($details)
	}
	renderMap = () => {
		const $map = document.createElement('game-map')
		$map.setAttribute('data', JSON.stringify(this.data))
		this.$root.querySelector('#map').innerHTML =''
		this.$root.querySelector('#map').append($map)
	}
}
