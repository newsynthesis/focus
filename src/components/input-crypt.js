import {log} from '../index.js'
import {openpgp} from '../crypt/index.js'

export default class InputCrypt extends HTMLElement {
	connectedCallback() {
		this.render()
	}
	handleSubmit = async (event) => {
		event.preventDefault()
		if (!this.password) {
			return
		}

		/* select what action to do */
		let action
		if (this.getAttribute('message')) {
			action = this.decryptMessage
		} else {
			action = this.encryptMessage
		}

		let res
		try {
			res = await action(
				this.getAttribute('message') || this.text,
				this.password
			)
		} catch(e) {
			log(e)
		}

		if (res && res.data) {
			log(res.data)
		} else if (res) {
			log(res)
		} else {
			log('#crypt error#')
		}
	}
	handleInput = (event) => {
		event.preventDefault()
		this[event.target.name] = event.target.value
	}
	render() {
		const message = this.getAttribute('message')
		let $form = document.createElement('form')
		$form.addEventListener('submit', this.handleSubmit)

		let $textArea = document.createElement('textarea')
		$textArea.setAttribute('placeholder', 'Message to encrypt')
		$textArea.setAttribute('name', 'text')
		$textArea.setAttribute('value', '')
		$textArea.addEventListener('input', this.handleInput)

		let $input = document.createElement('input')
		if (message) {
			$input.setAttribute('placeholder', 'Decryption password')
		} else {
			$input.setAttribute('placeholder', 'Encryption password')
		}
		$input.setAttribute('type', 'password')
		$input.setAttribute('name', 'password')
		$input.setAttribute('value', '')
		$input.addEventListener('input', this.handleInput)

		if (!message) {
			$form.append($textArea)
		}
		$form.append($input)
		this.append($form)
	}

	/* encryption methods */
	decryptMessage = async (base64Encrypted, passwords) => {
		const encrypted = atob(base64Encrypted)
		const encryptedMessage = await openpgp.readMessage({
			armoredMessage: encrypted
		})

		const res = await openpgp.decrypt({
			message: encryptedMessage,
			passwords: passwords,
			format: 'text'
		});
		return res
	}
	encryptMessage = async (text, passwords) => {
		const message = await openpgp.createMessage({ text: text})
		const encrypted = await openpgp.encrypt({
			message,
			passwords,
		})
		return btoa(encrypted)
	}
}
