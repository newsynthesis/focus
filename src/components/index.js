import * as rtc from './rtc/index.js'
import LogInfo from './log-info.js'
import InputCrypt from './input-crypt.js'
import SVGUser from './svg-user.js'
import GameMap from './game-map.js'

/* the visual user logs for the app */
customElements.define('log-info', LogInfo)

/* an input that talks to gpg */
customElements.define('input-crypt', InputCrypt)

/* a SVG map for the game */
customElements.define('game-map', GameMap)

/* a user conencted to a dom SVG element */
customElements.define('svg-user', SVGUser)

export {
	LogInfo,
	InputCrypt,
	SVGUser,
	GameMap,
}
