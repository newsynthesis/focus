export class SVGUser extends HTMLElement {
	static get observedAttributes() {
		return ['user', 'x', 'y']
	}
	/* x coordinate attr */
	set x(num) {
		this.setAttribute('x', num)
	}
	get x() {
		let num
		try {
			num = Number(this.getAttribute('x'))
		} catch (e) {
		}
		return num
	}
	/* y coordinate attr */
	set y(num) {
		this.setAttribute('y', num)
	}
	get y() {
		let num
		try {
			num = Number(this.getAttribute('y'))
		} catch (e) {
		}
		return num
	}
	/* user attr */
	set user(str) {
		this.setAttribute('user', str)
	}
	get user() {
		return this.getAttribute('user')
	}
	connectedCallback() {
		if (!this.user) {
			this.user = '?'
		}
	}
	attributeChangedCallback(attrName, oldVal, newVal) {
		this.render()
	}
	render() {
		this.innerHTML = ''
		const $user = document.createElement('span')
		$user.innerText = this.user
		this.append($user)
	}
}
export default SVGUser
