import {
  RsStorage,
  RsWidget,
  RsWidgetStatus,
} from 'https://cdn.skypack.dev/remote-storage-elements'

import moduleUser from './module-user.js'


class NotesStorage extends RsStorage {
  storageModule = moduleUser
}

customElements.define('rs-storage', NotesStorage)
customElements.define('rs-widget', RsWidget)
customElements.define('rs-widget-status', RsWidgetStatus)

export default {
  NotesStorage,
  RsWidget,
	RsWidgetStatus
}
