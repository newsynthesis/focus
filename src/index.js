import App from './app.js'
import * as components from './components/index.js'

/* our main app */
customElements.define('rtc-focus', App)

/* one logger we globally arround the app (imported from components) */
const $logger = document.createElement('log-info')
const log = $logger.log

/* export the logger element, and log function */
export {
	$logger,
	log,
}


export default {}
