const isDevelopment = window.location.origin.includes('localhost:')

/* for gitlab pages hosting user.gitlab.io/app/ */
const sitePathname =  isDevelopment ? '/' : '/focus/'

const host = 'localhost'
const port = 8080
const protocol = 'ws'
const url = `${protocol}://${host}:${port}`

const signalingServer = {
	host,
	port,
	protocol,
	url,
}
export {
	isDevelopment,
	sitePathname,
	signalingServer,
}
