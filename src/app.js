import './storage/index.js'
import AppRouter from './router/app.js'

customElements.define('app-router', AppRouter)

const App = class extends HTMLElement {
	connectedCallback() {
		console.log('**rtc-focus**')
		this.accessClaimDirectory = 'rtc-focus'
		this.render()
	}
	/* the main render of our application */
	render() {
		/* we use Remote Storage (RS), so user can save STATE, in their "own" location,
			 and no backend is required on the game part */
		let $rsStorage = document.createElement('rs-storage')
		if (this.debugStorage) $rsStorage.setAttribute('debug', true)
		$rsStorage.setAttribute('hidden', true)
		$rsStorage.setAttribute('access-claim-directory', this.accessClaimDirectory)
		$rsStorage.setAttribute('access-claim-rights', 'rw')
		$rsStorage.setAttribute('caching-enable-directory', `/${this.accessClaimDirectory}/`)
		$rsStorage.setAttribute('listen-connected', true)
		$rsStorage.setAttribute('listen-disconnected', true)
		$rsStorage.setAttribute('listen-network-online', true)
		$rsStorage.setAttribute('listen-network-offline', true)

		/* display globally a RS widget (to show if user is logged in or not) */
		let $rsWidgetStatus = document.createElement('rs-widget-status')
		let $rsWidgetLink = document.createElement('a')
		$rsWidgetLink.setAttribute('href', 'account')
		$rsWidgetLink.innerText = 'sync'

		$rsWidgetStatus.append($rsWidgetLink)

		/* the main "router" for this app; all /routes (pages) are defined from there */
		let $appRouter = document.createElement('app-router')

		/* let's put all our main elements in the page DOM */
		this.append($rsStorage)
		this.append($rsWidgetStatus)
		this.append($appRouter)
	}
}

export default App
